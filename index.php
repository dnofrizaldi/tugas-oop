<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    $sheep = new animal("shaun");

    echo "Name = " .$sheep->name ."<br>"; // "shaun"
    echo "legs = " .$sheep->legs ."<br>"; // 4
    echo "cold blooded = " .$sheep->cold_blooded."<br> <br>"; // "no"

    $kodok = new frog("buduk");
    echo "Name = " .$kodok->name ."<br>"; 
    echo "legs = " .$kodok->legs ."<br>"; 
    echo "cold blooded = " .$kodok->cold_blooded."<br>"; 
    echo "hop hop " .$kodok->gerak()."<br> <br>";

    $sungkong = new ape("kera sakti");
    echo "Name = " .$sungkong->name ."<br>"; 
    echo "legs = " .$sungkong->legs ."<br>"; 
    echo "cold blooded = " .$sungkong->cold_blooded."<br>"; 
    echo "Auooo " .$sungkong->suara()."<br> <br>";

?>